#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include "lsl_c.h"

// Removes the first N characters of a string
void chopN(char *str, size_t n)
{
    assert(n != 0 && str != 0);
    size_t len = strlen(str);
    if (n > len)
        return;  // Or: n = len;
    memmove(str, str+n, len - n + 1);
}

int main(){

  lsl_streaminfo info; /* OutStream Info */
  lsl_outlet outlet; /* The Outlet to Push the Markers */

  FILE *f;
  char *line = NULL;
  size_t size_line = 0;

  info = lsl_create_streaminfo("Xonotic-PipeStreamer", "Markers", 1, LSL_IRREGULAR_RATE, cft_string, "AABBBSSSCCCQQQDDD1987452");
  outlet = lsl_create_outlet(info,0,360);
  f = fopen("/home/simoeslo/.xonotic/data/server.cfg.log", "r");

  if (f == NULL){
    printf("Server Log Not Found Error!!");
    exit(1);
  }


  while(getline(&line, &size_line, f) != -1){

    double startClock = lsl_local_clock();
    chopN(line, 2); // Remove of the first 2 characters of the string.

    // We are only interested in logs that start with ":" so filter the rest.
    if(strncmp(line,":",1) == 0){
      printf("now sending: %s\n",line);

      double endClock = lsl_local_clock();
      /* now send it (note the &, since this function takes an array of char*) */
      lsl_push_sample_strt(outlet,(const char**)&line, (endClock+startClock)/2.0);
    }

    // Free Memory Procedure
    free(line);
    size_line = 0;
    line = NULL;

  }

  /* we never get here, buy anyway */
	lsl_destroy_outlet(outlet);

  return 0;
}
